# Fedora User Documentation Home

You can report Issues and submit Merge Requests for **Content Fixes** here. However, this repo is usually maintained internally. If you are unsure what to do, open a ticket.

General appearance issues and publishing issues should be reported against [/ fedora / Fedora Docs / Docs Website / Fedora Docs pages](https://gitlab.com/fedora/docs/docs-website/pages).

## How to edit these documents

All of this is written in AsciiDoc. It’s a simple mostly-plain-text
markup language. You may want to look at:


* [AsciiDoc Syntax Quick Reference](http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)
* [AsciiDoc Writer’s  Guide](http://asciidoctor.org/docs/asciidoc-writers-guide/)
* [Antora Documentation](https://docs.antora.org/antora/1.0/page/)

