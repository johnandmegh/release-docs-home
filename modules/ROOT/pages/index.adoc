= Fedora Rawhide User Documentation

Fedora documentation describes how to install and use the Fedora operating system and the software packaged by the Fedora Project.

This section contains three books about Fedora Rawhide:

* xref:rawhide@fedora:release-notes:index.adoc[Fedora Rawhide Release Notes]
* xref:rawhide@fedora:install-guide:index.adoc[Fedora Rawhide Installation Guide]
* xref:rawhide@fedora:system-administrators-guide:index.adoc[Fedora Rawhide System Administrator’s Guide]

The Fedora Documentation Project only actively maintains documentation for the most recent release and the one before it.
We also preserve older documentation on this site for historical interest and to acknowledge the generous contribution of time and effort by many, many writers and translators.
In 1997, Richard Stallman wrote:

____
"The biggest deficiency in free operating systems is not in the software — it is the lack of good free manuals that we can include in these systems.”footnote:[Free Software and Free Manuals, available from link:http://www.gnu.org/philosophy/free-doc.html[]]
____

We thank everybody who worked hard to correct that deficiency for previous versions of Fedora.
